# Lab 1 -- Introduction to the quality gates

[![pipeline status](https://gitlab.com/khaledismaeel/lab1-introduction-to-the-quality-gates-s22/badges/master/pipeline.svg)](https://gitlab.com/khaledismaeel/lab1-introduction-to-the-quality-gates-s22/-/commits/master)
[![coverage report](https://gitlab.com/khaledismaeel/lab1-introduction-to-the-quality-gates-s22/badges/master/coverage.svg)](https://gitlab.com/khaledismaeel/lab1-introduction-to-the-quality-gates-s22/-/commits/master)

The app is available at [https://sqrs-labs.herokuapp.com/hello](https://sqrs-labs.herokuapp.com/hello).
